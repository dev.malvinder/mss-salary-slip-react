# Salary Slip - Front End
### ReactJS

#### yarn install

#### yarn start
to run the project

Open: `localhost:3000`

API Port used is `3105` i.e. of `dev` environment
_Please change the API port according to your deployment_
It can be changed in `/src/helpers/apiUrls`


#### Modules Used

- React
- Redux
- Redux Thunk
- React Router
- React Router Redux

_You need to run the backend parallelly to view this application_


