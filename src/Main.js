import React, { Component } from 'react';
import { Route, Redirect } from 'react-router-dom';
import { Router } from 'react-router';

import SalarySlip from './components/SalarySlip';

export default class Main extends Component {
  render() {
    return (
      <Router history={this.props.history}>
        <div>
          <Route path="/" exact component={SalarySlip} />
        </div>
      </Router>
    );
  }
}