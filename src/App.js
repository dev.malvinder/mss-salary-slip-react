import React, { Component } from 'react';
import createHistory from 'history/createBrowserHistory';
import { Provider } from 'react-redux';

import Main from './Main';
import createStore from './reducers/create';


const history = createHistory();
const store = createStore(history, window.__INITIAL_STATE__);

class App extends Component {

  render() {
    return (
      <Provider store={store}>
        <Main history={history} />
      </Provider>
    );
  }
}

export default App;
