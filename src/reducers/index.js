import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

import salarySlip from './salarySlip';

export default combineReducers({
  routing: routerReducer,
  salarySlip
});
