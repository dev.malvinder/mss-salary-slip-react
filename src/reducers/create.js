import { createStore as _createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
// import cliendMiddleware from './clientMiddleware';
import { routerMiddleware } from 'react-router-redux';

import reducer from './index';


const createStore = (history, data) => {
  const reduxRouterMiddleware = routerMiddleware(history);
  const middleware = [thunk, reduxRouterMiddleware];

  let finalCreateStore;

  finalCreateStore = applyMiddleware(...middleware)(_createStore);

  const store = finalCreateStore(reducer, data);

  return store;
};

export default createStore;
