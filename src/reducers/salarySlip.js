import axios from 'axios';

import { salarySlip } from '../helpers/apiUrls';

const LOADING = 'LOADING';
const LOAD_SUCCESS = 'LOAD_SUCCESS';
const ERROR = 'ERROR';

const initialState = {
  loading: false,
  error: false,
  msg: '',
  salarySlip: {}
};

const reducer = (state = initialState, action = {}) => {
  switch (action.type) {
    default:
      return state;

    case LOADING:
      return {
        ...state,
        loading: true,
        error: false
      };

    case ERROR:
      return {
        ...state,
        loading: false,
        error: true,
        message: action.message
      };

    case LOAD_SUCCESS:
      return {
        ...state,
        loading: false,
        error: false,
        salarySlip: action.result,
        msg: action.message
      };
  }
};

export default reducer;


export const generateSalarySlip = payload => {
  return (async (dispatch, getState) => {

    dispatch({
      type: LOADING
    });

    try {
      const apiResponse = await axios.post(salarySlip.post, payload);

      dispatch({
        type: LOAD_SUCCESS,
        result: apiResponse,
        message: 'Success'
      });

    }
    catch(err) {
      console.error('Error in POST Salary Slip: ', err);
      dispatch({
        type: ERROR,
        message: 'Error in API'
      });
    }

  });
}
