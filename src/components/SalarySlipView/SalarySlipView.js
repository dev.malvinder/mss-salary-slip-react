import React, { Component } from 'react';
import { Card, Icon, Grid } from 'semantic-ui-react';

export default class SalarySlipView extends Component {
  render () {
    const slip = this.props.data;

    if (!slip.data) {
      return (
        <div></div>
      );
    }

    const data = slip.data.data;


    return (
      <div>
        <Card>
          <Card.Content>
            <Card.Header>
              {data.firstName} {data.lastName}
            </Card.Header>

            <Card.Meta>
              {data.paymentStartDate}
            </Card.Meta>

            <Card.Description>
              Super Amount: {data.superAmt}
            </Card.Description>
          </Card.Content>
          <Card.Content extra>
            <Grid columns={2}>
              <Grid.Column>
                <Icon name="dollar" color="green">
                   Net Income {data.netIncome}
                </Icon>
              </Grid.Column>
              <Grid.Column>
                <Icon name="dollar" color="red">
                  Payable Tax: {data.incomeTax}
                </Icon>
              </Grid.Column>
            </Grid>
          </Card.Content>
        </Card>
      </div>
    );
  }
}
