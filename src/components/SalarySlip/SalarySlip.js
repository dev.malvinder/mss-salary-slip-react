import React, { Component } from 'react';
import { Button, Grid, Input, Dimmer, Loader } from 'semantic-ui-react';
import { connect } from 'react-redux';

import SalarySlipView from '../SalarySlipView';
import { generateSalarySlip } from '../../reducers/salarySlip';

class SalarySlipClass extends Component {
  state = {
    firstName: "Malvinder",
    lastName: "Singh",
    annualSalary: 60050,
    superRate: 9,
    paymentStartDate: "1 March - 31 March"
  };

  handleChange = (event) => {
    const value = event.target.value;
    const name = event.target.name;

    switch (name) {
      case "firstName":
        this.setState({ firstName: value });
        break;

      case "lastName":
        this.setState({ lastName: value });
        break;

      case "annualSalary":
        this.setState({ annualSalary: value });
        break;

      case "superRate":
        this.setState({ superRate: value });
        break;

      case "paymentStartDate":
        this.setState({ paymentStartDate: value });
        break;
    }
  };

  generateSlip = () => {
    const { dispatch } = this.props;
    dispatch(generateSalarySlip(this.state));
  };


  render() {
    //console.log(this.state);
    const {
      loading,
      error,
      msg,
      data
    } = this.props;

    if (loading) {
      return (
        <Dimmer active>
          <Loader content="Loading" />
        </Dimmer>
      );
    }

    return (
      <Grid centred="true" columns={2}>
        <Grid.Row>
          <Grid.Column>
            <Input name="firstName" placeholder="First Name" onChange={this.handleChange} /><br />
            <Input name="lastName" placeholder="Last Name" onChange={this.handleChange} /><br />
            <Input name="annualSalary" type="number" placeholder="Annual Salary" onChange={this.handleChange} /><br />
            <Input name="superRate" type="number" placeholder="Super Rate" onChange={this.handleChange} /><br />
            <Input name="paymentStartDate" placeholder="Payment Date" onChange={this.handleChange} /><br /><br />
            <Button content="Generate Salary Slip" onClick={this.generateSlip} />
          </Grid.Column>
          <Grid.Column>
            {!error && <SalarySlipView data={data} />}
          </Grid.Column>
        </Grid.Row>
      </Grid>
    );
  }
}

const mapStateToProps= state => {
  return {
    loading: state.salarySlip.loading,
    error: state.salarySlip.error,
    msg: state.salarySlip.msg,
    data: state.salarySlip.salarySlip
  };
};

const SalarySlip = connect(mapStateToProps)(SalarySlipClass);

export default SalarySlip;
